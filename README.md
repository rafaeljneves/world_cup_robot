
# World Cup Robot

Hi! I'm a robot !     :)    And i am very smart and i know any year that happens the world cup... well on 21 Century .. at my version 1.0 . But i am learning...


## Getting started

Once you have [Python](https://www.python.org/downloads/) installed,  type on your terminal:
``` 
python world_cup_21st_century.py
```

## Wanna test me ?

If you input an year, i will tell you what country in Planet Earth that hosted the World Cup for that year. For example:

|                |INPUT                          |OUTPUT                         |
|----------------|-------------------------------|-----------------------------|
|Guest 1|`2002`            |['South Korea', 'Japan']            |
|Guest 2|`2006`            |'Germany'            |
|Guest 3          |`2014`            |'Brazil'            |
|Guest 4          |`2018`|'Russia'|

You are a Genius! Well Done !    :)

